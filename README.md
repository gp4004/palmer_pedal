# A simple palmer pedal mounting part for pedalboard
A 3d printed part to mount palmer pedals on a pedalboard using velcro whithout damaging the rubber pad

* the file is provided in openscad format
* just slip your palmer pedal into the mounting part
* stick your velcro strip on the mounting part

![photo](https://framapic.org/tg3VOuVya360/rkHIxTqzs9os.jpg)  

![openscad view](https://framapic.org/mHAjryD8Gqlz/QqxTPvPssGsQ.png)
