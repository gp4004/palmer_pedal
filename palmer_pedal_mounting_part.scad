width = 120.5;
length = 70;
height = 44;
$fn = 50;
thickness = 1.4;
grip = 5;

x = 90; //0 ou 90


rotate ([x,0,0])
difference()
{
    block();
    pedal();
    upperside();    
    cut();
    //bottom();
    back_round_hole();
    side_holes();
}

       
module side_holes()
{
    translate ([ 0, 25 , thickness + height / 2])  
    rotate ([0,90,0]) cylinder (r=height / 2 - 2* thickness, h=thickness * 2);       
    translate ([ width + thickness, 25 , thickness + height / 2])  
    rotate ([0,90,0]) cylinder (r=height / 2 - 2* thickness, h=thickness * 2);        
}


module pedal()
{
    translate ([thickness, thickness, thickness])
    color ([1,0,0]) cube ([ width, length, height]);
}


module block()
{   
    color ([1,1,1])
    cube ([ width + 2 * thickness, length + 2 * thickness, height + 2 * thickness]);
}

module upperside()
{   color ([0,1,0])
    translate ([thickness + grip, thickness + grip, height + thickness])
    cube ([ width - 2 * grip, length, thickness]);
}

module bottom()
{
        translate ([ 25, thickness + 1 , 24]) rotate ([90,0,0]) cylinder (r=height / 2 - 2* thickness, h=2 *thickness);   
        translate ([ 97, thickness + 1, 24]) rotate ([90,0,0]) cylinder (r=height / 2 - 2* thickness, h=2 * thickness);  
   
}



module back_round_hole()
{
        translate ([ width / 2, 25 , 0])  cylinder (r=height / 2 - 2* thickness, h=2 *thickness);   
}

module cut()
{
    color ([0,0,1])
    translate ([0, 50, 0])
    cube ([ width + 2 *
     thickness, length + 2 * thickness, height + 2 * thickness]);

}